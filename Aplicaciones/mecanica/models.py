from django.db import models

# Create your models here.
class Marca(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_marca = models.CharField(max_length=100)
    logo =models.FileField(upload_to='marca',null=True,blank=True)

    def __str__(self):
        return self.nombre_marca
class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    direccion = models.TextField()
    telefono = models.CharField(max_length=15)
    correo = models.EmailField()
    cedula = models.CharField(max_length=15, unique=True)
    foto=models.FileField(upload_to='cliente', null=True, blank=True)

    def __str__(self):
        return self.nombre


class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    cantidad_stock = models.PositiveIntegerField(default=0)
    fecha_entrada = models.DateField(auto_now_add=True)
    proveedor = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Servicios(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=150)
    costo = models.DecimalField(max_digits=10, decimal_places=2)
    nota = models.CharField(max_length=150)
    estado = models.CharField(max_length=50)  # Puedes ajustar la longitud según tus necesidades

    # Relación muchos a muchos con Producto
    productos = models.ManyToManyField(Producto, related_name='servicios_relacionados')

    def __str__(self):
        return f"{self.descripcion} - {self.costo} - {self.nota}"
class Mecanico(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=150)
    especialidad=models.CharField(max_length=150)
    telefono=models.CharField(max_length=10)
    correo=models.EmailField()
    foto=models.FileField(upload_to='mecanico', null=True, blank=True)

    def _str_(self):
        fila="{0}: {1} {2} - {3}"

class Vehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    modelo = models.CharField(max_length=50)
    placa = models.CharField(max_length=8, unique=True)
    kilometraje= models.TextField()
    estado = models.CharField(max_length=150)
    fecha_entrada = models.DateField(auto_now_add=True, null=True,blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    marca = models.ForeignKey(Marca,null=True,blank=True,on_delete=models.PROTECT)
    cliente = models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.PROTECT)
    mecanico = models.ForeignKey(Mecanico,null=True,blank=True,on_delete=models.PROTECT)
    # Relación muchos a muchos con Producto
    servicios = models.ManyToManyField(Servicios, related_name='servicios_relacionados')
    def __str__(self):
        return self.modelo



class Facturacion(models.Model):
    id_fac = models.AutoField(primary_key=True)
    fecha_emision = models.DateField(auto_now_add=True)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        fila = "{0}: Subtotal {1}, Total {2}"
        return fila.format(self.fecha_emision, self.subtotal, self.total)
