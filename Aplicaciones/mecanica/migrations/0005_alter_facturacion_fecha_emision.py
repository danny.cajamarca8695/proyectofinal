# Generated by Django 4.2.7 on 2024-02-08 16:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mecanica', '0004_facturacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facturacion',
            name='fecha_emision',
            field=models.DateField(auto_now_add=True),
        ),
    ]
