# Generated by Django 4.2.7 on 2024-02-11 17:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mecanica', '0009_producto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicios',
            name='mecanico',
        ),
    ]
