# <<<<<<< HEAD
from django.shortcuts import render, redirect, get_object_or_404
from .models import Mecanico, Facturacion, Servicios
from django.contrib import messages
# =======
from django.shortcuts import render
from .models import Marca
from .models import Vehiculo
from .models import Cliente
from .models import Producto
from decimal import Decimal
from django.db import IntegrityError
# >>>>>>> fecf9de (nueva plantilla)
# Create your views here.
def plantilla(request):
    return render(request,'cuerpo.html')
# =======
def listadoMarcas(request):
    marcaBdd=Marca.objects.all()
    return render(request,"marca/marca.html",{'marcas':marcaBdd})
def guardarMarca(request):
    nombre_marca= request.POST["nombre_marca"]
    logo=request.FILES.get("logo")
    nuevoMarcae=Marca.objects.create(nombre_marca=nombre_marca,
    logo=logo)
    messages.success(request, 'Marca guardada exitosamente')
    return redirect('/listadoMarcas/')
def eliminarMarca(request,id):
    marcaEliminar = Marca.objects.get(id=id)
    marcaEliminar.delete()
    messages.success(request, 'Marca eliminada exitosamente')
    return redirect('/listadoMarcas/')
def editarMarca(request,id):
    marcaEditar=Marca.objects.get(id=id)
    return render(request,'marca/editarMarca.html',{'marca':marcaEditar})
def procesarActualizarMarca(request):
    id=request.POST["id"]
    nombre_marca= request.POST["nombre_marca"]
    logo=request.FILES.get("logo")
    #Insertando datos mediante el ORM de DJANGO
    marcaEditar=Marca.objects.get(id=id)
    # Verificar si se proporcionó una nueva fotografía
    if logo:
        marcaEditar.logo = logo
    marcaEditar.nombre_marca=nombre_marca
    marcaEditar.save()
    messages.success(request,
      'Cliente ACTUALIZADO Exitosamente')
    return redirect('/listadoMarcas/')
    # cliente
def listadoClientes(request):
    clienteBdd=Cliente.objects.all()
    return render(request,"cliente/cliente.html",{'clientes':clienteBdd})
def guardarCliente(request):
    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        nombre = request.POST.get("nombre")
        direccion = request.POST.get("direccion")
        telefono = request.POST.get("telefono")
        correo = request.POST.get("correo")
        cedula = request.POST.get("cedula")
        foto = request.FILES.get("foto")

        try:
            # Creamos un nuevo objeto Cliente
            nuevoCliente = Cliente.objects.create(
                nombre=nombre,
                direccion=direccion,
                telefono=telefono,
                correo=correo,
                cedula=cedula,
                foto=foto
            )
            messages.success(request, 'Cliente guardado exitosamente')
            return redirect('/listadoClientes/')
        except IntegrityError:
            # Manejar la excepción de violación de restricción única
            messages.error(request, 'Error: Ya existe un cliente con esa cédula.')
    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, 'cuerpo.html', {})

    return render(request, 'cuerpo.html', {})
def eliminarCliente(request,id):
    clienteEliminar = Cliente.objects.get(id=id)
    clienteEliminar.delete()
    messages.warning(request, 'Cliente eliminada exitosamente')
    return redirect('/listadoClientes/')
def editarCliente(request,id):
    clienteEditar=Cliente.objects.get(id=id)
    return render(request,'cliente/editarCliente.html',{'cliente':clienteEditar})
def procesarActualizarCliente(request):
    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        id=request.POST["id"]
        nombre = request.POST.get("nombre")
        direccion = request.POST.get("direccion")
        telefono = request.POST.get("telefono")
        correo = request.POST.get("correo")
        cedula = request.POST.get("cedula")
        foto = request.FILES.get("foto")
        try:
            # Creamos un nuevo objeto Cliente
            clienteEditar=Cliente.objects.get(id=id)
            # Verificar si se proporcionó una nueva fotografía
            if foto:
                clienteEditar.foto = foto
            clienteEditar.nombre=nombre
            clienteEditar.direccion=direccion
            clienteEditar.correo=correo
            clienteEditar.cedula=cedula
            clienteEditar.telefono=telefono
            clienteEditar.save()
            messages.success(request, 'Cliente actualizado exitosamente')
            return redirect('/listadoClientes/')
        except IntegrityError:
            # Manejar la excepción de violación de restricción única
            messages.error(request, 'Error: Ya existe un cliente con esa cédula.')
    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, 'cuerpo.html', {})
    return render(request, 'cuerpo.html', {})
    #Insertando datos mediante el ORM de DJANGO

# Vehiculos
def listadoVehiculos(request):
    vehiculoBdd=Vehiculo.objects.all()
    marcaBdd=Marca.objects.all()
    clienteBdd=Cliente.objects.all()
    mecanicoBdd=Mecanico.objects.all()
    servicioBdd=Servicios.objects.all()
    return render(request,"vehiculo/vehiculo.html",{'vehiculos':vehiculoBdd,'marcas':marcaBdd,
    'clientes':clienteBdd,'mecanicos':mecanicoBdd,
    'servicios':servicioBdd})

def guardarVehiculo(request):
    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        modelo = request.POST.get("modelo")
        placa = request.POST.get("placa")
        kilometraje = request.POST.get("kilometraje")
        estado = request.POST.get("estado")
        total = request.POST.get("totalCosto")
        cliente = request.POST["cliente"]

        # transformo a objeto
        clienteSeleccionado = Cliente.objects.get(id=cliente)
        marca = request.POST["marca"]
        # transformo a objeto
        marcaSeleccionado = Marca.objects.get(id=marca)
        mecanico = request.POST["mecanico"]
        # transformo a objeto
        mecanicoSeleccionado = Mecanico.objects.get(id=mecanico)

        # Ahora obtenemos servicios_seleccionados del formulario
        servicios_seleccionados = [int(servicio_id) for servicio_id in request.POST.get("productos_seleccionados", "").split(",") if servicio_id]

        try:
            # Creamos un nuevo objeto Vehiculo
            nuevoVehiculo = Vehiculo.objects.create(
                modelo=modelo,
                placa=placa,
                kilometraje=kilometraje,
                estado=estado,
                total=total,
                cliente=clienteSeleccionado,
                marca=marcaSeleccionado,
                mecanico=mecanicoSeleccionado
            )

            # Asociamos los servicios seleccionados al nuevo vehículo
            nuevoVehiculo.servicios.set(servicios_seleccionados)

            messages.success(request, 'Vehiculo guardado exitosamente')
            return redirect('/listadoVehiculos/')
        except IntegrityError:
            # Manejar la excepción de violación de restricción única
            messages.error(request, 'Error: Ya existe un vehiculo con esa placa.')
            return redirect('/listadoVehiculos/')
    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, 'vehiculo/vehiculo.html', {})

    return render(request, 'cuerpo.html', {})
def eliminarVehiculo(request,id):
    vehiculoEliminar = Vehiculo.objects.get(id=id)
    vehiculoEliminar.delete()
    messages.warning(request, 'Vehiculo eliminada exitosamente')
    return redirect('/listadoVehiculos/')
def editarVehiculo(request,id):
    vehiculoEditar=Vehiculo.objects.get(id=id)
    marcaBdd=Marca.objects.all()
    clienteBdd=Cliente.objects.all()
    return render(request,'vehiculo/editarVehiculo.html',{'vehiculo':vehiculoEditar,'marcas':marcaBdd,'clientes':clienteBdd})
def procesarActualizarVehiculo(request):

    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        id=request.POST["id"]
        modelo = request.POST.get("modelo")
        placa = request.POST.get("placa")
        kilometraje = request.POST.get("kilometraje")
        estado = request.POST.get("estado")
        cliente= request.POST["cliente"]
        # trafomo a onjeto
        clienteSeleccionado=Cliente.objects.get(id=cliente)
        marca= request.POST["marca"]
        # trafomo a onjeto
        marcaSeleccionado=Marca.objects.get(id=marca)
        try:
            # Creamos un nuevo objeto Cliente
            vehiculoEditar=Vehiculo.objects.get(id=id)
            # Verificar si se proporcionó una nueva fotografía
            vehiculoEditar.modelo=modelo
            vehiculoEditar.placa=placa
            vehiculoEditar.kilometraje=kilometraje
            vehiculoEditar.estado=estado
            vehiculoEditar.cliente=clienteSeleccionado
            vehiculoEditar.marca=marcaSeleccionado
            vehiculoEditar.save()
            messages.success(request, 'Vehiculo actualizado exitosamente')
            return redirect('/listadoVehiculos/')
        except IntegrityError:
            # Manejar la excepción de violación de restricción única
            messages.error(request, 'Error: Ya existe un vehiculo con esa placa.')
            return redirect('/listadoVehiculos/')
    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, 'cuerpo.html', {})

    return render(request, 'cuerpo.html', {})
    #Insertando datos mediante el ORM de DJANGO
# PRODUCTOS
def listadoProductos(request):
    productoBdd=Producto.objects.all()
    return render(request,"producto/producto.html",{'productos':productoBdd})
def guardarProducto(request):
    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        nombre = request.POST.get("nombre")
        cantidad_stock = request.POST.get("cantidad_stock")
        proveedor = request.POST.get("proveedor")
        descripcion = request.POST.get("descripcion")
        try:
            # Creamos un nuevo objeto Cliente
            nuevoProducto = Producto.objects.create(
                nombre = nombre,
                cantidad_stock = cantidad_stock,
                proveedor = proveedor,
                descripcion = descripcion
            )
            messages.success(request, 'Producto guardado exitosamente')
            return redirect('/listadoProductos/')
        except IntegrityError:
            # Manejar la excepción de violación de restricción única
            messages.error(request, 'Error: Ya existe un vehiculo con esa placa.')
            return redirect('/listadoProductos/')
    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, '/vehiculo/vehiculo.html', {})

    return render(request, 'cuerpo.html', {})
def eliminarProducto(request,id):
    productoEliminar = Producto.objects.get(id=id)
    productoEliminar.delete()
    messages.warning(request, 'Producto eliminada exitosamente')
    return redirect('/listadoProductos/')
def editarProducto(request,id):
    productoEditar=Producto.objects.get(id=id)
    return render(request,'producto/editarProducto.html',{'producto':productoEditar})
def procesarActualizarProducto(request):
    if request.method == 'POST':
        # Obtenemos cada campo del formulario
        id = request.POST["id"]
        nombre = request.POST.get("nombre")
        cantidad_stock = int(request.POST['cantidad_stock'])
        proveedor = request.POST.get("proveedor")
        descripcion = request.POST.get("descripcion")

        productoEditar = Producto.objects.get(id=id)
        productoEditar.nombre = nombre
        productoEditar.cantidad_stock = cantidad_stock
        productoEditar.proveedor = proveedor
        productoEditar.descripcion = descripcion
        productoEditar.save()

        messages.success(request, 'Producto actualizado exitosamente')
        return redirect('/listadoProductos/')

    else:
        # Si es una solicitud GET, simplemente mostramos el formulario vacío
        return render(request, 'cuerpo.html', {})

    return render(request, 'cuerpo.html', {})
#servicios
def listadoServicios(request):
    serviciosBdd = Servicios.objects.all()
    productoBdd = Producto.objects.all()
    return render(request, "servicios/servicios.html", {'servicios': serviciosBdd, 'producto': productoBdd})
def guardarServicios(request):
    descripcion = request.POST.get("descripcion")
    costo = request.POST.get("costo")
    nota = request.POST.get("nota")
    estado = request.POST.get("estado")
    productos_seleccionados = request.POST.get("productos_seleccionados")

    # Convierte los valores a una lista de enteros
    productos_seleccionados = [int(producto_id) for producto_id in productos_seleccionados.split(",") if producto_id]

    nuevoServicio = Servicios.objects.create(
        descripcion=descripcion,
        costo=costo,
        nota=nota,
        estado=estado
    )

    # Asocia los productos seleccionados al nuevo servicio
    nuevoServicio.productos.set(productos_seleccionados)

    messages.success(request, 'Servicio Guardado Exitosamente')
    return redirect('/listadoServicios/')
def eliminarServicios(request,id):
    servicioEliminar=Servicios.objects.get(id=id)
    servicioEliminar.delete()
    messages.success(request,'Servico eliminado exitosamente')
    return redirect('/listadoServicios/')
def editarServicios(request, id):
    servicioEditar = Servicios.objects.get(id=id)
    productosBdd = Producto.objects.all()
    productos_seleccionados = ','.join(str(producto.id) for producto in servicioEditar.productos.all())

    return render(request, 'servicios/editarServicios.html', {'servicios': servicioEditar, 'productos': productosBdd, 'productos_seleccionados': productos_seleccionados})
def procesarActualizacionServicios(request):
    if request.method == "POST":
        # Verifica si el campo "id" está presente en la solicitud POST
        id_servicio = request.POST.get("id", None)

        if id_servicio is not None:
            descripcion = request.POST.get("descripcion")
            costo = request.POST.get("costo")
            nota = request.POST.get("nota")
            estado = request.POST.get("estado")
            productos_seleccionados = request.POST.get("productos_seleccionados")

            # Convierte los valores a una lista de enteros
            productos_seleccionados = [int(producto_id) for producto_id in productos_seleccionados.split(",") if producto_id]

            servicioEditar = get_object_or_404(Servicios, id=id_servicio)

            # Actualiza los campos del servicio
            servicioEditar.descripcion = descripcion
            servicioEditar.costo = costo
            servicioEditar.nota = nota
            servicioEditar.estado = estado

            # Asocia los productos seleccionados al nuevo servicio
            servicioEditar.productos.set(productos_seleccionados)
            servicioEditar.save()

            messages.success(request, 'Servicio ACTUALIZADO Exitosamente')
            return redirect('/listadoServicios/')

    # En caso de que no sea una solicitud POST o falte el ID
    messages.error(request, 'No se proporcionó un ID válido para la actualización del servicio.')
    return redirect('/listadoServicios/')
# >>>>>>> fecf9de (nueva plantilla)
# <<<<<<< Kerly
def listadoMecanico(request):
    mecanicosBdd = Mecanico.objects.all()
    return render(request, 'mecanico/mecanico.html', {'mecanicos': mecanicosBdd})
def guardarMecanico(request):
    nombre=request.POST["nombre"]
    especialidad=request.POST["especialidad"]
    telefono=request.POST["telefono"]
    correo=request.POST["correo"]
    foto=request.FILES.get("foto")
    messages.success(request, 'Mecanico Guardado Exitosamente')
    nuevoMecanico=Mecanico.objects.create(nombre=nombre,especialidad=especialidad,telefono=telefono,correo=correo,foto=foto)
    return redirect('/listadoMecanico/')
def eliminarMecanico(request,id):
    mecanicoEliminar=Mecanico.objects.get(id=id)
    mecanicoEliminar.delete()
    messages.success(request, 'Mecanico eliminado exitosamente')
    return redirect('/listadoMecanico/')
def editarMecanico(request,id):
    mecanicoEditar=Mecanico.objects.get(id=id)
    return render(request,
    'mecanico/editarMecanico.html',{'mecanico':mecanicoEditar})
def procesarActualizacionMecanico(request):
    id = request.POST["id"]
    nombre = request.POST["nombre"]
    especialidad = request.POST["especialidad"]
    telefono = request.POST["telefono"]
    correo = request.POST["correo"]
    foto = request.FILES.get("foto")

    # Obtener el objeto Mecanico después de la verificación de la foto
    mecanicoEditar = Mecanico.objects.get(id=id)

    # Verificar si se proporcionó una nueva fotografía
    if foto:
        mecanicoEditar.foto = foto

    # Actualizar los demás campos
    mecanicoEditar.nombre = nombre
    mecanicoEditar.especialidad = especialidad
    mecanicoEditar.telefono = telefono
    mecanicoEditar.correo = correo

    mecanicoEditar.save()
    messages.success(request, 'Mecanico ACTUALIZADO Exitosamente')
    return redirect('/listadoMecanico/')
def listadoFacturas(request):
    facturasBdd = Facturacion.objects.all()
    return render(request, 'facturas/facturacion.html', {'facturas': facturasBdd})
def guardarFacturas(request):
    subtotal=request.POST["subtotal"]
    total=request.POST["total"]
    messages.success(request, 'Factura Guardado Exitosamente')
    nuevoFactura=Facturacion.objects.create(subtotal=subtotal,total=total)
    return redirect('/listadoFacturas/')
def eliminarFacturas(request, id_fac):
    facturaEliminar = Facturacion.objects.get(id_fac=id_fac)
    facturaEliminar.delete()
    messages.success(request, 'Factura eliminada exitosamente')
    return redirect('/listadoFacturas/')
def editarFacturas(request, id_fac):
    factura = Facturacion.objects.get(id_fac=id_fac)
    return render(request, 'facturas/editarFacturacion.html', {'factura': factura})
def procesarActualizacionFactura(request):
    id_fac=request.POST["id_fac"]
    subtotal=request.POST["subtotal"]
    total=request.POST["total"]
    facturaEditar=Facturacion.objects.get(id_fac=id_fac)
    facturaEditar.subtotal=subtotal
    facturaEditar.total=total

    facturaEditar.save()
    messages.success(request,
      'Factura ACTUALIZADA Exitosamente')
    return redirect('/listadoFacturas/')
