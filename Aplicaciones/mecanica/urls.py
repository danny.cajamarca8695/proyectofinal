from django.urls import path,include
from . import views
urlpatterns = [
    path('', views.plantilla),
    path('listadoMarcas/',views.listadoMarcas),
    path('guardarMarca/',views.guardarMarca),
    path('eliminarMarca/<id>',views.eliminarMarca),
    path('editarMarca/<id>',views.editarMarca),
    path('procesarActualizarMarca/',views.procesarActualizarMarca),
    # cliente
    path('listadoClientes/',views.listadoClientes,name='listadoClientes/'),
    path('guardarCliente/',views.guardarCliente,name='guardarCliente/'),
    path('eliminarCliente/<id>',views.eliminarCliente,name='eliminarCliente/'),
    path('editarCliente/<id>',views.editarCliente,name='eliminarCliente/'),
    path('procesarActualizarCliente/',views.procesarActualizarCliente,name='procesarActualizarCliente/'),
    path('listadoMecanico/', views.listadoMecanico, name='listadomecanico/'),
    # vehiculo
    path('listadoVehiculos/',views.listadoVehiculos,name='listadoVehiculos/'),
    path('guardarVehiculo/',views.guardarVehiculo,name='guardarVehiculo/'),
    path('eliminarVehiculo/<id>',views.eliminarVehiculo,name='eliminarVehiculo/'),
    path('editarVehiculo/<id>',views.editarVehiculo,name='eliminarVehiculo/'),
    path('procesarActualizarVehiculo/',views.procesarActualizarVehiculo,name='procesarActualizarVehiculo/'),


    # PRODUCTO
    path('listadoProductos/',views.listadoProductos,name='listadoProductos/'),
    path('guardarProducto/',views.guardarProducto,name='guardarProducto/'),
    path('eliminarProducto/<id>',views.eliminarProducto,name='eliminarProducto/'),
    path('editarProducto/<id>',views.editarProducto,name='eliminarProducto/'),
    path('procesarActualizarProducto/',views.procesarActualizarProducto,name='procesarActualizarProducto/'),

    # Servicios
    path('listadoServicios/',views.listadoServicios, name='listadoServicios/'),
    path('guardarServicios/',views.guardarServicios, name='guardarServicios/'),
    path('eliminarServicios/<id>',views.eliminarServicios, name='eliminarServicios/'),
    path('editarServicios/<id>', views.editarServicios, name='editarServicios'),
    path('procesarActualizacionServicios/', views.procesarActualizacionServicios),

# <<<<<<< HEAD
    path('listadoMecanico/', views.listadoMecanico, name='listadomecanico/'),
    path('guardarMecanico/',views.guardarMecanico, name='guardarMecanico/'),
    path('eliminarMecanico/<id>',views.eliminarMecanico, name='eliminarMecanico'),
    path('editarMecanico/<id>',views.editarMecanico, name='editarMecanico'),
    path('procesarActualizacionMecanico/',views.procesarActualizacionMecanico,),
    path('listadoMarcas/',views.listadoMarcas),

    path('listadoFacturas/',views.listadoFacturas, name='listadoFacturas/'),
    path('guardarFacturas/',views.guardarFacturas, name='guardarFacturas/'),
    path('eliminarFacturas/<id_fac>',views.eliminarFacturas, name='eliminarFacturas'),
    path('editarFacturas/<id_fac>',views.editarFacturas, name='editarFacturas'),
    path('procesarActualizacionFactura/',views.procesarActualizacionFactura),
    path('listadoFacturas/',views.listadoFacturas),

    path('listadoServicios/',views.listadoServicios, name='listadoServicios/'),
    path('guardarServicios/',views.guardarServicios, name='guardarServicios/'),
    path('eliminarServicios/<id>',views.eliminarServicios, name='eliminarServicios'),
    ]
